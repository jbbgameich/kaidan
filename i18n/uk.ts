<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Простий дружній до користувачів клієнт Jabber/XMPP</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Ліцензування:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Переглянути початковий код у інтернеті</translation>
    </message>
    <message>
        <source>Report problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountSecurity</name>
    <message>
        <source>Account Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow to add new devices using a QR-Code, but never show the password as text. This action can not be undone, so consider storing the password in another way before using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable showing the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t expose the password in any form. This action can not be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable showing the QR-Code and password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountTransferPage</name>
    <message>
        <source>Transfer account to another device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan the QR code or enter the credentials as text on another device to log in on it.

Attention:
Never show this QR code to anyone else. It would allow unlimited access to your account!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chat address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Змінити пароль</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Поточний пароль:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Новий пароль:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Новий пароль (ще раз):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Вказані нові паролі не збігаються.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Поточний пароль є некоректним.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Вам слід встановити з&apos;єднання, щоб змінити ваш пароль.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Після зміни вашого пароля вам доведеться повторно ввести його на усіх інших ваших пристроях для спілкування.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Змінити</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation type="vanished">Скопіювати повідомлення</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation type="vanished">Редагування повідомлення</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Спойлер</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Отримати</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="vanished">Копіювати адресу даних</translation>
    </message>
    <message>
        <source>Quote</source>
        <translation type="vanished">Цитування</translation>
    </message>
</context>
<context>
    <name>ChatMessageContextMenu</name>
    <message>
        <source>Copy message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished">Копіювати адресу даних</translation>
    </message>
    <message>
        <source>Quote message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Створити повідомлення</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Зображення</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Відео</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Звук</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="vanished">Документ</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation type="vanished">Інший файл</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Виберіть файл</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Надіслати повідомлення-спойлер</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>Підказка до спойлера</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation>Увімкнути звук у сповіщеннях</translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation>Вимкнути звук у сповіщеннях</translation>
    </message>
    <message>
        <source>View profile</source>
        <translation>Переглянути профіль</translation>
    </message>
    <message>
        <source>Multimedia settings</source>
        <translation type="vanished">Параметри мультимедійних даних</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <source>Search up</source>
        <translation>Шукати вище</translation>
    </message>
    <message>
        <source>Search down</source>
        <translation>Шукати нижче</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>усі файли</translation>
    </message>
</context>
<context>
    <name>ClientWorker</name>
    <message>
        <source>Your account could not be deleted from the server. Therefore, it was also not removed from this app: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommonEncoderSettings</name>
    <message>
        <source>Very low</source>
        <translation>Дуже низька</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Низька</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Звичайна</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Висока</translation>
    </message>
    <message>
        <source>Very high</source>
        <translation>Дуже висока</translation>
    </message>
    <message>
        <source>Constant quality</source>
        <translation>Стала якість</translation>
    </message>
    <message>
        <source>Constant bit rate</source>
        <translation>Стала бітова швидкість</translation>
    </message>
    <message>
        <source>Average bit rate</source>
        <translation>Середня бітова швидкість</translation>
    </message>
    <message>
        <source>Two pass</source>
        <translation>Два проходи</translation>
    </message>
</context>
<context>
    <name>ConfirmationPage</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Скасувати</translation>
    </message>
</context>
<context>
    <name>DisablePasswordDisplay</name>
    <message>
        <source>Remove password completely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your password will not be included in the QR code anymore.
You won&apos;t be able to use the login via QR code without entering your password again because this action cannot be undone!
Make sure that you backed it up if you want to use your account later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisablePlainTextPasswordDisplay</name>
    <message>
        <source>Do not show password as text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your password will not be shown as text anymore.
You won&apos;t be able to see your password as text again because this action cannot be undone!
Make sure that you backed it up if you want to use your account later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Не вдалося зберегти файл %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Невдала спроба отримання: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation>Улюблене</translation>
    </message>
    <message>
        <source>People</source>
        <translation>Люди</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Природа</translation>
    </message>
    <message>
        <source>Food</source>
        <translation>Їжа</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation>Діяльність</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Подорожі</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Об&apos;єкти</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Символи</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Прапори</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation>Шукати емодзі</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Будь ласка, виберіть спілкування, щоб розпочати обмін повідомленнями</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Виберіть файл</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation type="vanished">Перейти до батьківської теки</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрити</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation type="vanished">Вийти</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Про програму</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Запросити друзів</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Посилання на запрошення скопійовано до буфера обміну даними</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Поза мережею</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>У мережі</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Встановлюємо з&apos;єднання…</translation>
    </message>
    <message>
        <source>Switch device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Посилання буде відкрито після встановлення з&apos;єднання.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="vanished">Не знайдено збереженого пароля. Будь ласка, введіть його.</translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation>Не знайдено коректного коду QR для входу.</translation>
    </message>
</context>
<context>
    <name>LocalAccountRemoval</name>
    <message>
        <source>Remove account from this app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your account will be removed from this app.
Your password will be deleted, make sure it is stored in a password manager or you know it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Увійти</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Увійдіть до вашого облікового запису XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation type="vanished">Ваш ідентифікатор Jabber:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation type="vanished">користувач@example.org</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation type="vanished">Ваш пароль:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Встановлення з&apos;єднання…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>З&apos;єднатися</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation type="vanished">Некоректне ім’я користувача або пароль.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation type="vanished">Не вдалося встановити з&apos;єднання із сервером. Будь ласка, перевірте, чи маєте ви з&apos;єднання із інтернетом.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation type="vanished">На сервері не передбачено можливості захищених з&apos;єднань.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation type="vanished">Помилка під час спроби встановлення захищеного з&apos;єднання.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation type="vanished">Не вдалося визначити адресу сервера. Будь ласка, перевірте, чи правильно вказано ідентифікатор Jabber.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="vanished">Не вдалося встановити з’єднання із віддаленим сервером.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="vanished">На сервері не передбачено підтримки протоколу розпізнавання.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation type="vanished">Сталася невідома помилка. Будь ласка, ознайомтеся із журналом.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="vanished">Вхід за допомогою QR-коду</translation>
    </message>
</context>
<context>
    <name>MediaRecorder</name>
    <message>
        <source>Default</source>
        <translation>Типовий</translation>
    </message>
</context>
<context>
    <name>MediaUtils</name>
    <message>
        <source>Take picture</source>
        <translation>Зробити знімок</translation>
    </message>
    <message>
        <source>Record video</source>
        <translation>Записати відео</translation>
    </message>
    <message>
        <source>Record voice</source>
        <translation>Записати голосове повідомлення</translation>
    </message>
    <message>
        <source>Send location</source>
        <translation>Надіслати адресу</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>Виберіть файл</translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>Виберіть зображення</translation>
    </message>
    <message>
        <source>Choose video</source>
        <translation>Виберіть відео</translation>
    </message>
    <message>
        <source>Choose audio file</source>
        <translation>Виберіть звуковий файл</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>Виберіть документ</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>усі файли</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>зображення</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>відео</translation>
    </message>
    <message>
        <source>Audio files</source>
        <translation>файли звукових даних</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation>документи</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">Не вдалося надіслати повідомлення через те, що немає з&apos;єднання.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation type="vanished">Не вдалося виправити повідомлення через те, що немає з&apos;єднання.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="vanished">Спойлер</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Повідомлення не вдалося надіслати.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>Спроба виправити повідомлення зазнала невдачі.</translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delivered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Помилка</translation>
    </message>
</context>
<context>
    <name>MultimediaSettings</name>
    <message>
        <source>Multimedia Settings</source>
        <translation>Параметри мультимедійних даних</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation>Налаштувати</translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation>Захоплення зображень</translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation>Запис звуку</translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation>Запис відео</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation>Відеокамера</translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation>Звуковий вхід</translation>
    </message>
    <message>
        <source>Container</source>
        <translation>Контейнер</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Зображення</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Звук</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Відео</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation>Кодек</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Роздільність</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation>Якість</translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation>Частота дискретизації</translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation>Частота кадрів</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation>Ініціалізація…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Немає даних</translation>
    </message>
    <message>
        <source>Recording...</source>
        <translation>Запис…</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Призупинено</translation>
    </message>
</context>
<context>
    <name>MultimediaSettingsPage</name>
    <message>
        <source>Multimedia Settings</source>
        <translation type="vanished">Параметри мультимедійних даних</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation type="vanished">Налаштувати</translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation type="vanished">Захоплення зображень</translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation type="vanished">Запис звуку</translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation type="vanished">Запис відео</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="vanished">Відеокамера</translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation type="vanished">Звуковий вхід</translation>
    </message>
    <message>
        <source>Container</source>
        <translation type="vanished">Контейнер</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Зображення</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Звук</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Відео</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation type="vanished">Кодек</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">Роздільність</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation type="vanished">Якість</translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation type="vanished">Частота дискретизації</translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation type="vanished">Частота кадрів</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="vanished">Готово</translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation type="vanished">Ініціалізація…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation type="vanished">Немає даних</translation>
    </message>
    <message>
        <source>Recording...</source>
        <translation type="vanished">Запис…</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation type="vanished">Призупинено</translation>
    </message>
</context>
<context>
    <name>NewMedia</name>
    <message>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation>Ініціалізація…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Немає даних</translation>
    </message>
    <message>
        <source>Recording... %1</source>
        <translation>Запис… %1</translation>
    </message>
    <message>
        <source>Paused %1</source>
        <translation>Призупинено %1</translation>
    </message>
</context>
<context>
    <name>PasswordRemovalPage</name>
    <message>
        <source>Remove password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can decide to only not show your password for &lt;b&gt;%1&lt;/b&gt; as text anymore or to remove it completely from the account transfer. If you remove your password completely, you won&apos;t be able to use the account transfer via scanning without entering your password because it is also removed from the QR code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show password as text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove completely</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Mark as read</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation>Доступний</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation>Вільний для спілкування</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Відсутність</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation>Не турбувати</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation>Довга відсутність</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Поза мережею</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="vanished">Невидимий</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Некоректне ім’я користувача або пароль.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Не вдалося встановити з&apos;єднання із сервером. Будь ласка, перевірте, чи маєте ви з&apos;єднання із інтернетом.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>На сервері не передбачено можливості захищених з&apos;єднань.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Помилка під час спроби встановлення захищеного з&apos;єднання.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your server name.</source>
        <translation>Не вдалося визначити адресу сервера. Будь ласка, перевірте, чи правильно вказано назву сервера.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="vanished">Не вдалося встановити з’єднання із сервером.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>На сервері не передбачено підтримки протоколу розпізнавання.</translation>
    </message>
    <message>
        <source>This server does not support registration.</source>
        <translation>На сервері не передбачено підтримки реєстрації.</translation>
    </message>
    <message>
        <source>An unknown error occured.</source>
        <translation type="vanished">Сталася невідома помилка.</translation>
    </message>
    <message>
        <source>The server is offline or blocked by a firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection could not be refreshed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The internet access is not permitted. Please check your system&apos;s internet access configuration.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeGenerator</name>
    <message>
        <source>Generating the QR code failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation>Сканувати QR-код</translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation>Немає доступної відеокамери.</translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation>Із відеокамерою працює інша програма.
Спробуйте завершити роботу інших програм, які користуються відеокамерою.</translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation>Підримки формату відеокамери «%1» не передбачено.</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan the QR code from your existing device to transfer your account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show explanation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continue without QR code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationLoginDecisionPage</name>
    <message>
        <source>Set up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Register a new account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an existing account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>Пароль успішно змінено.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Не вдалося змінити пароль: %1</translation>
    </message>
</context>
<context>
    <name>RemoteAccountDeletion</name>
    <message>
        <source>Delete account completely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your account will be deleted completely, which means from this app and from the server.
You will not be able to use your account again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished">Вилучити</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Додати новий контакт</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Унаслідок цього також буде надіслано запит щодо доступу до даних щодо присутності контакту у системі.</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Ідентифікатор Jabber:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>користувач@example.org</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Псевдонім:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Додаткове повідомлення:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Повідомте вашому співрозмовнику дещо про вас.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Помилка: будь ласка, перевірте, чи правильно вказано ідентифікатор Jabber.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Доступний</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Вільний для спілкування</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Відсутність</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Не турбувати</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Довга відсутність</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Поза мережею</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Помилка</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Не вдалося додати контакт через те, що немає з&apos;єднання.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Не вдалося вилучити контакт через те, що немає з&apos;єднання.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Спойлер</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation>Не вдалося перейменувати запис контакту через те, що не вдалося встановити з&apos;єднання.</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Встановлення з’єднання…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Контакти</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Додати новий контакт</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Поза мережею</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation>Шукати контакти</translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Ви справді хочете вилучити запис контакту &lt;b&gt;%1&lt;/b&gt; із вашої книги записів контактів?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Вилучити контакт</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Вилучити</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation>Перейменувати запис контакту</translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="vanished">Редагування назви:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Перейменувати</translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation>Підпис</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Надіслати</translation>
    </message>
</context>
<context>
    <name>ServerListModel</name>
    <message>
        <source>Custom server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No limitation</source>
        <extracomment>Unlimited file size for uploading files
----------
Deletion of message history saved on server</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsContent</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Параметри</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="unfinished">Змінити пароль</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="unfinished">Змінити пароль до вашого облікового запису. Вам доведеться повторно ввести цей пароль на усіх інших ваших пристроях для спілкування.</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation type="unfinished">Параметри мультимедійних даних</translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation type="unfinished">Налаштуйте параметри обробки фотографій, відео та звукових записів</translation>
    </message>
    <message>
        <source>Account security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>configure whether this device can be used to log in on a new device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove account from Kaidan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove account from this app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete account from the server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="vanished">Змінити пароль</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="vanished">Змінити пароль до вашого облікового запису. Вам доведеться повторно ввести цей пароль на усіх інших ваших пристроях для спілкування.</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation type="vanished">Параметри мультимедійних даних</translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation type="vanished">Налаштуйте параметри обробки фотографій, відео та звукових записів</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Enjoy free communication on every device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Let&apos;s start</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Запит щодо підписки</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Вами отримано запити щодо підписки від &lt;b&gt;%1&lt;/b&gt;. Якщо ви приймете його, відповідний обліковий запис матиме доступ до вашого стану присутності у системі.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Відхилити</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Прийняти</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Не вдалося надіслати файл через те, що немає з&apos;єднання.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation type="unfinished">Повідомлення не вдалося надіслати.</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation>Профіль</translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Псевдонім</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Про програму</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Ел. пошта</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>День народження</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Сайт</translation>
    </message>
</context>
</TS>
