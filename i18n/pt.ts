<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Um cliente Jabber/XMPP simples e fácil de usar</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licença:</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="obsolete">Código-fonte no GitHub</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Ver código-fonte online</translation>
    </message>
    <message>
        <source>Report problems</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutSheet</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="vanished">Um simples, fácil de usar cliente de Jabber/XMPP</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licença:</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="vanished">Código-fonte no GitHub</translation>
    </message>
</context>
<context>
    <name>AccountSecurity</name>
    <message>
        <source>Account Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow to add new devices using a QR-Code, but never show the password as text. This action can not be undone, so consider storing the password in another way before using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable showing the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t expose the password in any form. This action can not be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable showing the QR-Code and password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountTransferPage</name>
    <message>
        <source>Transfer account to another device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan the QR code or enter the credentials as text on another device to log in on it.

Attention:
Never show this QR code to anyone else. It would allow unlimited access to your account!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chat address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <source>Navigate Back</source>
        <translation type="vanished">Navegue De Volta</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Mudar a palavra-passe</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Palavra-passe atual:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Nova palavra-passe:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Nova palavra-passe (repetição):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Novas palavras-passe não coincidem.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>A palavra-passe atual é inválida.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Você tem que estar conectado para alterar sua palavra-passe.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatMessageContextMenu</name>
    <message>
        <source>Copy message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quote message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Compor mensagem</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Enviar</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Imagem</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Vídeo</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Áudio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="vanished">Documento</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation type="vanished">Outro ficheiro</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Seleccione um ficheiro</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClientWorker</name>
    <message>
        <source>Your account could not be deleted from the server. Therefore, it was also not removed from this app: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommonEncoderSettings</name>
    <message>
        <source>Very low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very high</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Constant quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Constant bit rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average bit rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two pass</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmationPage</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContextDrawer</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Ações</translation>
    </message>
</context>
<context>
    <name>DisablePasswordDisplay</name>
    <message>
        <source>Remove password completely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your password will not be included in the QR code anymore.
You won&apos;t be able to use the login via QR code without entering your password again because this action cannot be undone!
Make sure that you backed it up if you want to use your account later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisablePlainTextPasswordDisplay</name>
    <message>
        <source>Do not show password as text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your password will not be shown as text anymore.
You won&apos;t be able to see your password as text again because this action cannot be undone!
Make sure that you backed it up if you want to use your account later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Food</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Selecione um bate-papo para começar a enviar mensagens</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation type="obsolete">Seleccione um ficheiro</translation>
    </message>
</context>
<context>
    <name>ForwardButton</name>
    <message>
        <source>Navigate Forward</source>
        <translation type="vanished">Navegar Para A Frente</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Back</source>
        <translation type="vanished">Voltar</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation type="vanished">Sair</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Convide amigos</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Link de convite copiado para a área de transferência</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Configurações</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Offline</translation>
    </message>
    <message>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>O link será aberto depois de você ter se conectado.</translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocalAccountRemoval</name>
    <message>
        <source>Remove account from this app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your account will be removed from this app.
Your password will be deleted, make sure it is stored in a password manager or you know it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Inicie sessão na sua conta XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation type="vanished">O teu Jabber-ID:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">A sua diáspora*-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation type="vanished">user@example.org</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">user@diaspora.pod</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation type="vanished">Sua Senha:</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>A ligar…</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation type="vanished">Nome de usuário ou senha inválidos.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation type="vanished">Não é possível conectar ao servidor. Por favor, verifique a sua ligação à Internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation type="vanished">O servidor não suporta conexões seguras.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation type="vanished">Erro ao tentar conectar com segurança.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation type="vanished">Não foi possível resolver o endereço do servidor. Por favor, verifique o seu JID novamente.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="vanished">Não consegui ligar-me ao servidor.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="vanished">Protocolo de autenticação não suportado pelo servidor.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation type="vanished">Ocorreu um erro desconhecido; consulte o registo para mais detalhes.</translation>
    </message>
</context>
<context>
    <name>MediaRecorder</name>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaUtils</name>
    <message>
        <source>Take picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Record video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Record voice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">Não foi possível enviar mensagem, como resultado de não estar conectado.</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delivered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultimediaSettings</name>
    <message>
        <source>Multimedia Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Container</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="unfinished">Imagem</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished">Áudio</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="unfinished">Vídeo</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recording...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paused</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultimediaSettingsPage</name>
    <message>
        <source>Image</source>
        <translation type="obsolete">Imagem</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">Áudio</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Vídeo</translation>
    </message>
</context>
<context>
    <name>NewMedia</name>
    <message>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recording... %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paused %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordRemovalPage</name>
    <message>
        <source>Remove password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can decide to only not show your password for &lt;b&gt;%1&lt;/b&gt; as text anymore or to remove it completely from the account transfer. If you remove your password completely, you won&apos;t be able to use the account transfer via scanning without entering your password because it is also removed from the QR code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show password as text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove completely</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Mark as read</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Offline</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation type="unfinished">Nome de usuário ou senha inválidos.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation type="unfinished">Não é possível conectar ao servidor. Por favor, verifique a sua ligação à Internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation type="unfinished">O servidor não suporta conexões seguras.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation type="unfinished">Erro ao tentar conectar com segurança.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your server name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="obsolete">Não consegui ligar-me ao servidor.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="unfinished">Protocolo de autenticação não suportado pelo servidor.</translation>
    </message>
    <message>
        <source>This server does not support registration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server is offline or blocked by a firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection could not be refreshed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The internet access is not permitted. Please check your system&apos;s internet access configuration.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeGenerator</name>
    <message>
        <source>Generating the QR code failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan the QR code from your existing device to transfer your account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show explanation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continue without QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationLoginDecisionPage</name>
    <message>
        <source>Set up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Register a new account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use an existing account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RemoteAccountDeletion</name>
    <message>
        <source>Delete account completely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your account will be deleted completely, which means from this app and from the server.
You will not be able to use your account again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation type="unfinished">Adicionar novo contacto</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation type="unfinished">user@example.org</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Offline</source>
        <translation type="obsolete">Offline</translation>
    </message>
    <message>
        <source>Error: Please check the JID.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Não foi possível adicionar contacto, por não estar ligado.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Não foi possível remover o contato, como resultado de não estar conectado.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Contacts</source>
        <translation>Contatos</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Adicionar novo contacto</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Conectando…</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Send</source>
        <translation type="unfinished">Enviar</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerListModel</name>
    <message>
        <source>Custom server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No limitation</source>
        <extracomment>Unlimited file size for uploading files
----------
Deletion of message history saved on server</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsContent</name>
    <message>
        <source>Change password</source>
        <translation type="unfinished">Mudar a palavra-passe</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Configurações</translation>
    </message>
    <message>
        <source>Account security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>configure whether this device can be used to log in on a new device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove account from Kaidan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove account from this app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete account from the server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="obsolete">Mudar a palavra-passe</translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <source>Settings</source>
        <translation type="vanished">Configurações</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Enjoy free communication on every device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Let&apos;s start</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Não foi possível enviar ficheiro, como resultado de não estar conectado.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Ficheiro</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Sobre</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Correio eletrónico</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Sítio Web</translation>
    </message>
</context>
</TS>
